bash-openssl
===

Alpine image with `bash` and `openssl`, running as user `1000`, a member of group `0`.
