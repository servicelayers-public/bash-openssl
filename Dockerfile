FROM bash

RUN adduser -u 1000 -g "" -G root -D app &&\
  apk add --update --no-cache \
    gzip \
    python3 \
    tar \
    openssl

USER 1000

ARG IMAGE_COMMIT_ARG=""
ENV IMAGE_COMMIT=${IMAGE_COMMIT_ARG}
ARG IMAGE_TAG_ARG=""
ENV IMAGE_TAG=${IMAGE_TAG_ARG}
